# Bibliographique critique
Exercice de génération d'une bibliographie critique à imprimer réalisée dans le cadre du séminaire collectif de doctorat de Benoît Melançon, Université de Montréal, 2019.

## Fonctionnement
Chaque modification met à jour la page web disponible ici : [https://antoinentl.gitlab.io/bibliographie-critique/](https://antoinentl.gitlab.io/bibliographie-critique/).
Cette page est à imprimer via un navigateur web (si possible Chrome ou Chromium).

Quelques informations sur le fonctionnement du processus :

- le fichier `index.md` intègre la bibliographie avec le bout de code `{% bibliography --file bibliographie.bib %}` ;
- la bibliographie est comprise dans un fichier `_bibliographie/bibliographie.bib` qui est un export BibLaTeX depuis [Zotero](https://www.zotero.org/) avec l'extension [Better BibTeX](https://retorque.re/zotero-better-bibtex/) ;
- [la page HTML](https://antoinentl.gitlab.io/bibliographie-critique/) est générée avec le générateur de site statique [Jekyll](https://jekyllrb.com/) accompagné de [jekyll-microtypo](https://github.com/borisschapira/jekyll-microtypo/) et [jekyll-scholar](https://github.com/inukshuk/jekyll-scholar) ;
- le mini site web est déployé avec [GitLab Pages](https://about.gitlab.com/product/pages/) ;
- [paged.js](https://www.pagedmedia.org/paged-js/) permet de mettre en forme la page web pour qu'elle soit imprimée (idéalement avec Chrome ou Chromium).

## Duplication
Pour réutiliser ce dépôt, il _suffit_ de le forker puis de modifier le fichier `index.md` et la bibliographie.

## Contact
Antoine Fauchié : [antoine@quaternum.net](mailto:antoine@quaternum.net), [www.quaternum.net](https://www.quaternum.net), [@antoinentl](https://gitlab.com/antoinentl).
