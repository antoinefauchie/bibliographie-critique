---
layout: default
title: "Bibliographie critique"
---
## Bibliographie critique – Antoine Fauchié

{% bibliography --file bibliographie.bib %}

### À propos de cette bibliographie
Bibliographie critique à imprimer réalisée dans le cadre du séminaire collectif de doctorat de Benoît Melançon, Université de Montréal, 2019.

Style bibliographique utilisé : Université de Montréal - APA (French - Canada) ([source](https://www.zotero.org/styles?q=id%3Auniversite-de-montreal-apa)).

Bibliographie créée avec les composants suivants : Markdown, Zotero, Git, Jekyll, jekyll-microtypo, jekyll-scholar, paged.js, GitLab et GitLab Pages.
Si vous lisez la version imprimée, vous pouvez découvrir la version en ligne : [https://antoinefauchie.gitpages.huma-num.fr/bibliographie-critique/](https://antoine.gitpages.huma-num.fr/bibliographie-critique/).
Code source : [https://gitlab.huma-num.fr/antoinefauchie/bibliographie-critique](https://gitlab.huma-num.fr/antoine/bibliographie-critique).

CC BY-SA – Antoine Fauchié – antoine.fauchie@umontreal.ca – [https://www.quaternum.net](https://www.quaternum.net)
